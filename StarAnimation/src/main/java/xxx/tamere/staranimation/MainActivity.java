package xxx.tamere.staranimation;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.FrameLayout;

public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";

    FrameLayout container;
    Fragment marioFragment;
    Fragment luigiFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container = (FrameLayout)findViewById(R.id.container);
        marioFragment = Fragment.instantiate(this, MarioFragment.class.getName());
        luigiFragment = Fragment.instantiate(this, LuigiFragment.class.getName());

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, marioFragment)
                    .commit();
        }
    }

    public void startAnimation(float rawX, float rawY) {
        StarAnimationView star = new StarAnimationView(this, 200, 4);
        int translation = star.getScaledSize() / 2;

        // Check the size of the ActionBar + StatusBar
        // We need this becaus the raw coordinates given by the MotionEvent are the coordinate
        // in the whole window (actionBar+statusbar included).

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int topOffset = dm.heightPixels - container.getHeight();

        // Add the animation view in the container:
        star.setX(rawX - translation);
        star.setY(rawY - translation - topOffset);
        container.addView(star);
    }

    public void showLuigi() {
        getFragmentManager()
                .beginTransaction()
                .addToBackStack("luigi")
                .replace(R.id.fragment_container, luigiFragment)
                .commit();
    }
}

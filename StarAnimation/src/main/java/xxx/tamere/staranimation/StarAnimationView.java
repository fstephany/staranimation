package xxx.tamere.staranimation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collection;

public class StarAnimationView extends RelativeLayout {
    private static final String TAG = "StarAnimationView";
    private static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
    private static final int sAnimationDuration = 600;

    private Collection<Animator> starAnimations;

    private int scaledBoxSize;
    private int numberOfStars;

    Drawable starDrawable;
    int drawableWidth;

    public StarAnimationView(Context context, int boxSize, int numberOfStars) {
        super(context);
        float densityFactor = context.getResources().getDisplayMetrics().density;

        this.scaledBoxSize = (int) (boxSize * densityFactor);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(scaledBoxSize, scaledBoxSize);
        this.setLayoutParams(params);
        this.numberOfStars = numberOfStars;

        starDrawable = getResources().getDrawable(R.drawable.star);
        drawableWidth = starDrawable.getIntrinsicWidth();
    }

    @Override
    protected void onAttachedToWindow () {
        super.onAttachedToWindow();
        startAnimation();
    }

    public int getScaledSize() {
        return scaledBoxSize;
    }

    private void startAnimation() {
        int boxCenter = this.scaledBoxSize / 2;

        AnimatorSet animatorSet = new AnimatorSet();
        starAnimations = new ArrayList<Animator>(numberOfStars);

        for(int i = 0; i < numberOfStars; i++) {
            ImageView star = new ImageView(getContext());

            RelativeLayout.LayoutParams params =
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            star.setLayoutParams(params);
            star.setImageDrawable(starDrawable);
            star.setAdjustViewBounds(true);
            this.addView(star);

            star.setX(boxCenter - drawableWidth / 2);
            star.setY(boxCenter - drawableWidth / 2);

            // Polar coordinates.
            // We need to remove half_drawable_width because the polar coordinates are computed
            // for the center of the drawable, not the top-left corner.
            // x = r * cos(theta) - half_of_drawable_width
            // y = r * cos(theta) - half_of_drawable_width
            double r = scaledBoxSize / 2 - drawableWidth / 2;
            double theta = i * Math.PI * 2 / numberOfStars;
            double x = r * Math.cos(theta) - drawableWidth / 2;
            double y = r * Math.sin(theta) - drawableWidth / 2;

            // We now need to translate the values because the center is not in (0, 0)
            // but in (boxCenter, boxCenter)
            double finalX = x + boxCenter;
            double finalY = y + boxCenter;

            PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("x", (float)finalX);
            PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("y", (float) finalY);
            ObjectAnimator starAnimator  = ObjectAnimator.ofPropertyValuesHolder(star, pvhX, pvhY);

            starAnimations.add(starAnimator);
        }

        animatorSet.playTogether(starAnimations);
        animatorSet.setInterpolator(sDecelerator);
        animatorSet.setDuration(sAnimationDuration);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                ViewGroup mummy = (ViewGroup) StarAnimationView.this.getParent();
                mummy.removeView(StarAnimationView.this);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                ViewGroup mummy = (ViewGroup) StarAnimationView.this.getParent();
                mummy.removeView(StarAnimationView.this);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });

        animatorSet.start();
    }
 }

package xxx.tamere.staranimation;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MarioFragment extends Fragment {
    private static String TAG = "MarioFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mario, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final MainActivity mainActivity = (MainActivity)getActivity();
        Button toLuigi = (Button)view.findViewById(R.id.move_to_luigi);

        toLuigi.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Beware that getRawX will give the absolute coordinates of the event,
                    // ActionBar and Status Bar included.
                    mainActivity.startAnimation(event.getRawX(), event.getRawY());
                }

                return false;
            }
        });

        toLuigi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showLuigi();
            }
        });
    }
}
